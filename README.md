# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Running locally ##

### Docker ###
The project uses Laradock (http://laradock.io/).
Laradock relies on Docker, you can download this from here: https://store.docker.com/editions/community/docker-ce-desktop-mac

Once cloned:
```
npm install

cd laradock-tt
cp env-example .env
```

Start the docker application that you downloaded

If you are on windows you will need to share the relevant drive your code lives on: https://github.com/moby/moby/issues/23992

To run docker (in root):
```
npm run docker:start
```
or (in laradock-tt directory):
```
docker-compose up -d apache2 mysql-tte
```

The first time this can take a while.

Once it has built you can get into the workspace (like running vagrant ssh) with:
```
npm run docker:workspace
```
Is the equivalent of: ```docker-compose exec workspace bash``` in the laradock-tt directory

*NB: On windows you may need to be in powershell to do this (if you receive the error: 'unable to setup input stream: unable to set IO streams as raw terminal: The handle is invalid.')*

This is where you can run artisan, composer etc.

If you want to enter the other containers (e.g the mysql one):
```
docker-compose exec {container-name} bash
docker-compose exec mysql bash
```

To stop the containers running you can run (this will stop, not delete):
```
npm run docker:stop
```
is the equivalent of: ```docker-compose stop```

For xdebug docs: http://laradock.io/documentation/#start-stop-xdebug

If any changes made to the docker file / config of a specific container you can rebuild:
```
#Rebuild all
docker-compose build

#Rebuild specific
docker-compose build php-fpm apache2 mysql-tte
```

### Hosts ###
Add 127.0.0.1    trussel-trust-emails.local.reason.digital to your /etc/hosts file
```
sudo /bin/sh -c 'echo "127.0.0.1 trussel-trust-emails.local.reason.digital" >> /etc/hosts'
```

## Installing
Copy the `.env.example` to `.env` and add your database creds.

Change `APP_ENV=dev` to `APP_ENV=local`

Like any other Laravel app run `composer install` in the root directory (within the workspace).

Also run `php artisan key:generate` to generate a new key.

Run `php artisan migrate` to migrate the databases

For dummy data you can run `php artisan db:seed`

Run `php artisan storage:link` to set up the symlinks for uploaded media

Run `npm run dev` (outside of the workspace) to generate all the front end assets

#### Troubleshooting on Windows
Windows likes to have problems so here's a few issues I encountered:

If you run the `npm install` command inside the workspace and it fails because Python is not installed, run:
```
apt-get update
apt-get install -y python2.7
npm config set python /usr/bin/python2.7
```
This should install and config Python in the workspace.

If the `npm install` command fails due to an "EACCES: permission denied" error, run:
```
npm config set user 0
```

If `npm run dev` fails due to a "Node Sass does not yet support your current environment" error, run:
```
npm rebuild node-sass
```

## Deployments
@todo
